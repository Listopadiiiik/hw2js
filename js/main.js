// try..catch ми використовуємо, щоб перевірити алгоритм коду. Наприклад, якщо ми проходимось по масиву і
// шукаємо якесь значення. Його може не бути, але код все одно спрацює, і джаваскріпт надасть неіснуючому значенню 
// undefined. В такому випадку нам потрібно створити свою помилку (помилка не в коді, а в алгоритмі, яка полягає в тому,
// що немає значення). Як от в цьому завданні, де ми перевіряли властивості об'єкту і створили свої помилки
// 
// Ще один приклад - коли ми використовуемо prompt і користувач вводить "неправильні" за умовою в prompt значення.
// В такому випадку JS видасть щоь накшталт NaN чи Null. Замість них ми створюємо свою помилку, яка повідомляє 
// користувачу, що він введено некоректні дані.

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const root = document.querySelector('#root');
root.insertAdjacentHTML('afterbegin', '<ul class="list"></ul>');
const list = document.querySelector('.list');

books.forEach((item) => {
  try { 
    if (!item.author) {
      throw new Error(`${JSON.stringify(item)} doesn't have author`);
    } else if (!item.name) {
      throw new Error(`${JSON.stringify(item)} doesn't have name`);
    } else if (!item.price) {
      throw new Error(`${JSON.stringify(item)} doesn't have price`);
    } else if (item.name != undefined && item.price != undefined && item.author != undefined) {
      list.insertAdjacentHTML('afterbegin', `<li>Книга "${item.name}" написана ${item.author} коштує ${item.price}</li>`);
    }
  } catch(e) {
    console.log(e);
  }
});